FROM mariolopjr/jekyll:latest
MAINTAINER mario@techmunchies.net

COPY Gemfile.dev westpointrealtors-theme /

RUN set -ex \
  && apk add --no-cache git \
  && bundle install --gemfile /Gemfile.dev

VOLUME /srv
EXPOSE 4000

WORKDIR /srv
ENTRYPOINT ["jekyll"]
