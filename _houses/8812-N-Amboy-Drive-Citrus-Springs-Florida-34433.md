---
layout: house
address: "8812 N Amboy Drive, Citrus Springs, Florida, 34433"
price: 93000
status: "Available"
list_date: 2016-10-26
bedrooms: 2
bathrooms: 2
garages: 2
home_size: 1548
lot_size: 10454
year: 1988
housing_type: "Single Family Home"
num_of_stories: 1
hoa_fees: 0
hud_case_num: 093-616994
cover_image: "Picture1.jpg"
images:
  - "Picture1.jpg"
  - "Picture2.jpg"
  - "Picture3.jpg"
  - "Picture4.jpg"
  - "Picture5.jpg"
coming_soon: false
homepage: true
archived: false
---
