"use strict";

// Load gulp
var gulp = require('gulp');

// Loads gulp plugins using $.pluginname
var $ = require('gulp-load-plugins')();

// Parallelize Operations
var parallel = require('concurrent-transform');
var os       = require("os");

// Load BrowserSync
var browserSync = require('browser-sync');

// Imagemin Plugins
var giflossy = require('imagemin-giflossy');
var mozjpeg  = require('imagemin-mozjpeg');
var pngquant = require('imagemin-pngquant');
var webp     = require('imagemin-webp');

// Load delete plugin
var del = require('del');

// Load FTP plugin
var ftp = require('vinyl-ftp');

// Resize images
gulp.task('resize', () => {
    return gulp.src('optimize/**/*.{jpg,png}')
        .pipe(parallel(
            $.imageResize({
                width : 1000,
                crop : false,
                quality: 1,
                upscale : false
            }),
            os.cpus().length
        ))
        .pipe(gulp.dest('optimized/responsive'));
});

// Minify and Compress images (webp not supported yet)
gulp.task('images', ['resize'], () => {
    return gulp.src(['optimize/**/*.{gif,svg,tiff}', 'optimized/responsive/**/*.{jpg,png}'])
        .pipe(parallel(
            $.imagemin([
                $.imagemin.gifsicle({interlaced: true}),
                $.imagemin.jpegtran({progressive: true}),
                $.imagemin.optipng(),
                $.imagemin.svgo({svgoPlugins: [{removeViewBox: false}]}),
                giflossy({optimizationLevel: 3, lossy: 80}),
                mozjpeg({quality: '70'}),
                pngquant({quality: '70-80'})//,
                //webp({quality: '50', alphaQuality: '50'}) // Not supported by latest gulp-imagemin, doesn't use correct ext ... maybe it works now?
            ]),
            os.cpus().length
        ))
        .pipe(gulp.dest('optimized/compressed'));
});

// Minify and Compress images -- outputting webp files
gulp.task('webp', ['images'], () => {
    return gulp.src(['optimize/**/*.{tiff}', 'optimized/responsive/**/*.{jpg,png}'])
        .pipe(parallel(
            $.webp({quality: 50, alphaQuality: 50}),
            os.cpus().length
        ))
        .pipe(gulp.dest('optimized/compressed'));
});

// Clean all temporary directories from build process
gulp.task('clean:temp', ['webp'], () => {
    return del([
        'optimized/responsive/**/**'
    ]);
});

// Deploy Site to Production
gulp.task('deploy:master', () => {

    var glob = 'build/**/*.{css,html,jpg,js,json,png,svg,tiff,xml,webp}';

    var conn = ftp.create({
        host: process.env.FTP_HOST,
        user: process.env.FTP_PROD_USER,
        password: process.env.FTP_PROD_PASS,
        parallel: 5,
        maxConnections: 5,
        log: $.util.log,
        secure: true
    });

    return gulp.src(glob, { base: 'build/', buffer: false } )
        //.pipe(conn.clean('/**', 'build/', { base: 'build/' } ))
        .pipe(conn.newer('/'))
        .pipe(conn.dest('/'));
});

// Deploy Site to Development
gulp.task('deploy:development', () => {

    var glob = 'build/**/*.{css,html,jpg,js,json,png,svg,tiff,xml,webp}';

    var conn = ftp.create({
        host: process.env.FTP_HOST,
        user: process.env.FTP_DEV_USER,
        password: process.env.FTP_DEV_PASS,
        parallel: 5,
        maxConnections: 5,
        log: $.util.log,
        secure: true
    });

    return gulp.src(glob, { base: 'build/', buffer: false } )
        //.pipe(conn.clean('/**', 'build/', { base: 'build/' } ))
        .pipe(conn.newer('/'))
        .pipe(conn.dest('/'));
});

// Default gulp task
gulp.task('default', ['images', 'clean:temp']);

// Optimize production site
gulp.task('optimize', ['images', 'webp', 'clean:temp']);
